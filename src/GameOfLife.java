//  Şemsi Yiğit Özgümüş
// 2012400207
// yigitozgumus1@gmail.com
// CMPE436 - Assignment1
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;

/**
 * Created by yigitozgumus on 10/3/16.
 */
public class GameOfLife {

    private Integer[][] grid;
    private Integer[][] currentPopulation;
    private Integer M;
    private Integer N;
    private Integer maxGen;

    // Main structure of the program
    public static void main(String[] args) throws IOException {
        GameOfLife GoL = new GameOfLife();
        GoL.initialize(args);
        GoL.iterateGenerations();
        GoL.outputResult(GoL.grid,GoL.currentPopulation);
    }
    // Initialization of the dataset. If input.txt is present, import else generate population
    public void initialize(String[] args) throws IOException {
        M = Integer.parseInt(args[0]);
        N = Integer.parseInt(args[1]);
        maxGen = Integer.parseInt(args[2]);
        grid = new Integer[M][N];
        if (args.length == 3){
            generatePopulation();
        }else{
            importPopulation(args[3]);
        }

    }
    // Population import, reading from file and importing to the grid.
    public void importPopulation(String fileName) throws IOException {
        try(BufferedReader br = new BufferedReader(new FileReader(fileName))){
            StringBuilder sb = new StringBuilder();
            //Get the first line
            String line = br.readLine();
            String[] larray = line.split("\\s+");
            Integer NSub = larray.length;
            Integer MSub = 0;
            if (NSub != N){
                throw new IllegalArgumentException("N and columns of input.txt does not match.");
            }
            while(line != null){
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
                MSub +=1;
            }
            if (MSub != M){
                throw new IllegalArgumentException("M and rows of input.txt does not match.");
            }
            String allGrid = sb.toString();
            allGrid.replace("\n"," ");
            String[] tempGrids = allGrid.split("\\s+");
            for (int i = 0; i < M ; i++) {
                for (int j = 0; j < N; j++) {
                    grid[i][j] = Integer.parseInt(tempGrids[i * N + j]);
                }
            }
        }
        // Check cells for illegal cell values, (anything other than 0 or 1)
        validateImport();
    }
    public void validateImport(){
        for (int i = 0; i <grid.length ; i++) {
            for (int j = 0; j <grid[0].length ; j++) {
                if(!(grid[i][j] == 0 || grid[i][j] == 1)){
                    throw new IllegalArgumentException("Illegal Cell Value");
                }
            }
        }
    }
    // Generate population with fixed seed randomization.
    public void generatePopulation(){
        Random rand = new Random(112358);
        for (int i = 0; i < M ; i++) {
            for (int j = 0; j < N; j++) {
                grid[i][j] = rand.nextInt(2);
            }
        }
    }
    // this function updates the population number of iterations
    public void iterateGenerations(){
        Integer[][] tempPopulation = new Integer[M][N];
        currentPopulation = grid.clone();
        for (int i = 0; i < maxGen ; i++) {
          currentPopulation = (Integer[][]) updatePopulation(currentPopulation,tempPopulation);
        }
    }
    // this function updates the cell values according to neighbor status
    public Object updatePopulation(Integer[][] source,Integer[][]dest){
        for (int i = 0; i < source.length ; i++) {
            for (int j = 0; j < source[0].length ; j++) {
                dest[i][j] = checkNeighbors(source,i,j);
            }
        }
        return dest;
    }
    //Applying game of life rules and checking neighbors is done on this method
    public int checkNeighbors(Integer[][] target,int row,int col){
        int rowMaxLimit = target.length-1;
        int colMaxLimit = target[0].length-1;
        int rowMinLimit = 0;
        int colMinLimit = 0;
        int count = 0;
        int rowStart = Math.max(rowMinLimit,row-1);
        int rowEnd = Math.min(rowMaxLimit,row+1);
        int colStart = Math.max(colMinLimit,col-1);
        int colEnd = Math.min(colMaxLimit,col+1);

        for (int i = rowStart; i <=rowEnd ; i++) {
            for (int j = colStart; j <= colEnd ; j++) {
                count += target[i][j];
            }
        }
        count -= target[row][col];
        if (target[row][col] == 1){
            if (count == 2 || count == 3) {return 1;}else{return 0;}
        }else{
            if(count ==3){return 1;}else{return 0;}
        }
    }

    // Output the results
    public void outputResult(Integer[][] start,Integer[][] finish){
        System.out.println("State of the population in the 1. generation:\n");
        printPopulation(start);
        System.out.println("State of the population in the "+maxGen+". generation:\n");
        printPopulation(finish);

    }
    // Utility function for printing the population.
    public void printPopulation(Integer[][] target){
        for (int i = 0; i < M ; i++) {
            for (int j = 0; j < N ; j++) {
                System.out.print(target[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }


}
