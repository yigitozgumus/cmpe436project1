//  Şemsi Yiğit Özgümüş
// 2012400207
// yigitozgumus1@gmail.com
// CMPE436 - Assignment1
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.StringJoiner;


/**
 * Created by yigitozgumus on 10/3/16.
 */
public class MatrixMultiply {
    private Integer[] rows;
    private Integer[] cols;
    private Integer[][] matrixA;
    private Integer[][] matrixB;
    private String[][] resultMatrix;
    private String outputFileName ;
    //Main structure of the program
    public static void main(String[] args) throws IOException {
        MatrixMultiply MM = new MatrixMultiply();
        MM.readMatrices(args);
        MM.Multiply();
        MM.outputResult();
    }
    // Reads the matrices from the files and imports them to the matrix arrays
    public void readMatrices(String[] files) throws IOException {
        // Initialize the row col arrays
        rows = new Integer[2];
        cols = new Integer[2];
        outputFileName = files[2];
        // Loop for 2 input matrices
        for (int i = 0; i <files.length-1 ; i++) {
            try(BufferedReader br = new BufferedReader(new FileReader(files[i]))) {
                StringBuilder sb = new StringBuilder();
                //Get the first line
                String line = br.readLine();
                // get the rows and columns
                String[] rowcol = line.split("\\s+");
                rows[i] = Integer.parseInt(rowcol[0]);
                cols[i] = Integer.parseInt(rowcol[1]);
                // Choose which matrix to be initialized
                if (i == 0) { matrixA = new Integer[rows[i]][cols[i]]; }else{ matrixB = new Integer[rows[i]][cols[i]];}
                line = br.readLine();
                while (line != null) {
                    sb.append(line);
                    sb.append(System.lineSeparator());
                    line = br.readLine();
                }
                String allMatrix = sb.toString();
                allMatrix.replace("\n","");
                String[] tempLines = allMatrix.split("\\s+");
                Integer[][] temp = new Integer[rows[i]][cols[i]];
                for (int j = 0; j <rows[i] ; j++) {
                    for (int k = 0; k <cols[i] ; k++) {
                        temp[j][k] = Integer.parseInt(tempLines[j*cols[i] + k]);
                    }
                }
                if (i == 0) { matrixA = temp; }else{ matrixB = temp;}
            }

        } // Illegal argument check
        if (cols[0] != rows[1]) {
            throw new IllegalArgumentException("A:Cols: " + cols[0] + " did not match B:Rows " + rows[1] + ".");
        }else{
            System.out.println("Input data is imported.");
        }

    } // Basic n3 loop that executes the multiplication process
    public void Multiply(){
        // initialize the result matrix
        resultMatrix = new String[rows[0]][cols[1]];

        for (int i = 0; i <rows[0] ; i++) {
            for (int j = 0; j <cols[1] ; j++) {
                Integer temp = 0;
                for (int k = 0; k <cols[0] ; k++) {
                    temp += matrixA[i][k] * matrixB[k][j];
                }
                resultMatrix[i][j] = String.valueOf(temp);
            }
        }
        System.out.println("Computation is completed.");

    }
    // This method outputs the result to the result file
    public void outputResult() throws FileNotFoundException, UnsupportedEncodingException {
       Path outputFile = Paths.get(outputFileName);
        String delimiter = System.getProperty("line.separator");
        StringJoiner sb = new StringJoiner(delimiter);
        for (String[] row : resultMatrix)
            sb.add(Arrays.toString(row));
        String s = sb.toString().replaceAll("\\[|\\]|\\,","");
        PrintWriter pw = new PrintWriter(outputFile.toString(),"UTF-8");
        pw.print(s);
        pw.close();
        System.out.println("The result is written to the " + outputFileName + ".");

    }

}
