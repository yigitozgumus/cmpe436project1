# README #

This is the first project for the Cmpe 436 : Concurrent & Distributed Programming course

## General information

* To run the matrix multiply you should supply three names for the 2 input matrices and the solution matrix.
The example run of the program is given below

``` bash

java matrixMultiply matrixA matrixB matrixC

```

* To run the gameOfLife program you must supply minimum of 3 arguments. M, N and maximum number of generation. If you add population file, the program automatically imports the population and checks whether the M and N values you supplied match the ones in the input file. You can see the run examples below 
``` bash 

java gameOfLife 5 5 25 

java gameOfLife 5 5 25 input.txt

```